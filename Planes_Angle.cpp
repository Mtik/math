#include <iostream>
#include <cmath>

class vector {
public: 
    vector();
    vector(double x, double y);
    vector(double x, double y, double z);
    friend double operator*(const vector& a, const vector& b);
    double getAnglePlanes(const vector& a, const vector& b);
private:
    double x, y, z;
};
vector::vector() {}
vector::vector(double x, double y) : x{ x }, y{ y } {}
vector::vector(double x, double y, double z) : x{ x }, y{ y }, z { z } {}
double operator*(const vector& a, const vector& b) {
    return (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
}
double vector::getAnglePlanes(const vector& a, const vector& b) {
    return acos(((a.x * b.x) + (a.y * b.y) + (a.z * b.z)) / 
            (sqrt(a.x*a.x + a.y*a.y + a.z*a.z) * 
             sqrt(b.x*b.x + b.y*b.y + b.z*b.z)));
}

int main() {
    vector a(1,5,4);
    vector b(2,4,3);
    vector c;
    std::cout << "Planes angle = "  << c.getAnglePlanes(a,b);
    return 0; 
}
