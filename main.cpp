#include <iostream>

class vector {
public: 
    vector(double x, double y);
    vector(double x, double y, double z);
    friend double operator*(vector a, vector b);
private:
    double x, y, z;
};

vector::vector(double x, double y) : x{ x }, y{ y } {}
vector::vector(double x, double y, double z) : x{ x }, y{ y }, z { z } {}
double operator*(vector a, vector b) {
    return (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
}


int main() {
    vector a(1,5,4);
    vector b(2,4,3);
    std::cout << a*b;
    return 0; 
}
